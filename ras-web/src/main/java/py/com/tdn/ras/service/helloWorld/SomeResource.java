package py.com.tdn.ras.service.helloWorld;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("SomeResource")
@Produces("text/plain")
public class SomeResource {
	
	@GET
	public String dogetAsPlanText(){
		return "text/plain";
		
	}

	@GET
	@Produces("text/html")
	public String doGetAsHtml(){
		return "text/html";
		
	}
	
}
