package py.com.tdn.ras.service.helloWorld;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/helloWorld")
public class HelloWorld {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String saludar () {
		return "HOLA MUNDO";
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("{nombre}")
	public String saludarNombre(@PathParam("nombre") String nombre) {
		return "Hola " + nombre;
	}
}
