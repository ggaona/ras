package py.com.tnd.ras.service.proceso;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.StyledEditorKit.BoldAction;
import javax.ws.rs.Path;

import py.com.tdn.dao.ProcesoDAO;
import py.com.tdn.dao.VentaBean;
import py.com.tdn.ras.model.VentaDetalle;


@WebServlet ("/carrito")
public class CarritoServlet extends HttpServlet {
	
	@EJB
	VentaBean venta;
	
	protected void doGet (HttpServletRequest request, HttpServletResponse response){
		 System.out.println("En el servlet");
		//int idVenta =  Integer.parseInt(request.getParameter("idVenta"));
		//int cantidad = Integer.parseInt(request.getParameter("monto"));
		//int monto = Integer.parseInt(request.getParameter("cantidad"));	
		
		
		String nombre = request.getParameter("nombre");
		String apellido = request.getParameter("apellido");
		Boolean guardar = Boolean.parseBoolean(request.getParameter("guardar"));
		
		//VentaDetalle ventaDetalle = new VentaDetalle();
		//ventaDetalle.setCantidad(cantidad);
		//ventaDetalle.setMonto(monto);
		
		//venta.agregarDetalle(ventaDetalle, guardar);
		venta.guardarCLiente(nombre, apellido, guardar);
	}
}
