package py.com.tnd.ras.service.proceso;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.com.tdn.dao.ProcesoDAO;
import py.com.tdn.ras.model.*;

/**
 * Created by gaonag on 02/01/15.
 */
@Path("/proceso")
@RequestScoped
public class ProcesoService {

	@EJB
	ProcesoDAO dao;

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerRoutingNumber()throws Exception {
	    //List<Cliente> clientes = dao.getListaClientes(null);
		//System.out.println("Cli: " + clientes.size());
	    List<Cliente> clientes = dao.getListaClientesConVenta();
		return Response.ok().entity(clientes).build();
	}

}
