package py.com.tnd.ras.service.helloworld;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("helloworld")
public class HelloWorld {
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String saludar() {
		return "HOLA MUNDO";
	}
	
	@GET
	@Path("{nombre}")
	@Produces(MediaType.TEXT_PLAIN)
	public String saludarNombre(@PathParam("nombre") String nombre)  {
		return "HOLA: " + nombre;
	}

}
