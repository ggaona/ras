package py.com.tnd.ras.service.helloworld;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/myResource")
@Produces("text/plain")
public class SomeResource {
	@GET
	public String doGetAsPlainText(){
		return "doGetAsPlainText";
	}

	@GET
	@Produces("text/html")
	public void doGetAsHtml() {
		//return "doGetAsHtml";
	}
}

