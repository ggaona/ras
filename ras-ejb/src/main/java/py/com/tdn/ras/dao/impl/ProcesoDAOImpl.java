package py.com.tdn.ras.dao.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import py.com.tdn.dao.ProcesoDAO;
import py.com.tdn.ras.model.*;

/**
 * Session Bean implementation class ProcesoDAOImpl
 */
@Stateless
@LocalBean
public class ProcesoDAOImpl implements ProcesoDAO {

	@PersistenceContext(unitName = "ras-model")
	private EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public ProcesoDAOImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Proceso getByDescripcion(String descripcion) {
		Proceso proceso = null;
		System.out.println("llamada a GET BY DESCRIPCON:__" + descripcion
				+ "___");
		try {
			proceso = (Proceso) this.entityManager
					.createQuery(
							"Select c from Proceso c where c.descripcion like :descripcion")
					.setParameter("descripcion", descripcion.trim())
					.getSingleResult();
		} catch (Exception nre) {
			nre.printStackTrace();
		}
		return proceso;
	}

	public List<Cliente> getListaClientes(String nombreCliente) {
		List<Cliente> cliente = null;

		cliente = this.entityManager
				.createQuery(
						"select c from Cliente c "
								+ "where c.nombre = :nombreCliente or :nombreCliente is null")
				.setParameter("nombreCliente", nombreCliente).getResultList();

		return cliente;
	}

	public List<Cliente> getListaClientesConVenta() {
		List<Cliente> cliente = null;

		cliente = this.entityManager.createQuery(
				"select c from Cliente c where c.venta is not empty")
				.getResultList();

		return cliente;

	}
}
