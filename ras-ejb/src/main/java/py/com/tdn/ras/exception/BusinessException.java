package py.com.tdn.ras.exception;

import javax.ejb.ApplicationException;

import org.jboss.logging.Logger;

/**
 * Clase base para las excepciones de la capa EJB
 * 
 * @author Gabriela Gaona <gabriela.gaona@konecta.com.py>
 * 
 */
@ApplicationException(rollback = true)
public class BusinessException extends Exception {

	private static final long serialVersionUID = 3691414767644533855L;

	/**
	 * Construye la excepci�n
	 * 
	 * @param msg
	 *            Mensaje que incorpora informaci�n extra a la excepci�n
	 **/
	public BusinessException(String msg, Class<?> clazz) {

		super(msg);
		Logger logger = org.jboss.logging.Logger.getLogger(clazz);
		logger.error(msg);
	}

	/**
	 * Construye la excepci�n
	 * 
	 * @param msg
	 *            Mensaje que incorpora informaci�n extra a la excepci�n
	 * @param cause
	 *            La causa de la excepci�n
	 **/
	public BusinessException(String msg, Throwable cause) {

		super(msg, cause);
	}

}
