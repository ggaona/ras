package py.com.tdn.ras.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.swing.text.StyledEditorKit.BoldAction;

import org.hibernate.mapping.Array;

import py.com.tdn.dao.VentaBean;
import py.com.tdn.ras.model.*;

@Stateful
@LocalBean
public class VentaBeanImp implements VentaBean {

	@PersistenceContext(unitName = "ras-model")
	private EntityManager entityManager;

	public VentaBeanImp() {

	}

	@Override
	public void agregarDetalle(VentaDetalle ventaDetalle, Boolean guardar) {
		
		System.out.println("Estoy en el Bean");
		
		Venta venta = this.entityManager.find(Venta.class, 1);
		
		if (venta.getVentaDetalle() == null) {
			System.out.println("El detalle es nulo");
		}
		if (venta.getVentaDetalle().isEmpty()) {
			
			System.out.println("El detalle esta vacio");
			
			List<VentaDetalle> ventaDetalleList = new ArrayList<VentaDetalle>();
			ventaDetalleList.add(ventaDetalle);
			venta.setVentaDetalle(ventaDetalleList);
		}
		else {
			venta.getVentaDetalle().add(ventaDetalle);
		}
		
		if (guardar) {
			this.entityManager.persist(ventaDetalle);
		}
	}
	
	@Override
	public void guardarCLiente(String nombre, String apellido, Boolean guardar) {
		
		System.out.println(nombre);
		System.out.println(apellido);
	  
		Cliente cliente = new Cliente();
		cliente.setNombre(nombre);
		cliente.setApellido(apellido);
		
		this.entityManager.persist(cliente);
		
		
	}
	
	@AroundInvoke
	public Object interceptor(InvocationContext context) throws Exception {
		context.getParameters();
		
		for (int i = 0; i < context.getParameters().length - 1; i++) {
			System.out.println(i);
			
			if (context.getParameters()[i].equals(String.class)) {
				
				context.getParameters()[i] = ((String) context.getParameters()[i]).toUpperCase();	
			}
		
		}
		
	    System.out.println(" interceptando ");
	    
		return context.proceed();
	}
}
