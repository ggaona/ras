package py.com.tdn.ras.dao.impl;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceContext;

import py.com.tdn.dao.BasicDAO;

public class BasicDAOImp<T, PK extends Serializable> implements BasicDAO<T, PK> {

	@PersistenceContext(unitName = "ras-model")
	private EntityManager entityManager;
	protected Class<T> entityClass;

	public BasicDAOImp() {

		ParameterizedType genericSuperclass = (ParameterizedType) getClass()
				.getGenericSuperclass();

		this.entityClass = (Class<T>) genericSuperclass
				.getActualTypeArguments()[0];

	}

	@Override
	public T detachEntity(T entity) {

		this.entityManager.detach(entity);

		for (Field f : this.entityClass.getDeclaredFields()) {
			if (f.isAnnotationPresent(OneToMany.class)
					|| f.isAnnotationPresent(ManyToMany.class)) {
				try {
					f.set(entity, null);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (f.isAnnotationPresent(ManyToOne.class)
					|| f.isAnnotationPresent(OneToOne.class)) {
				try {
					entityManager.detach(f.get(entity));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		return entity;
	}

	@Override
	public List<T> detachEntities(List<T> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T persist(T t) {
		this.entityManager.persist(t);
		this.entityManager.flush();
		this.detachEntity(t);
		return t;
	}

	@Override
	public T merge(T t) {
		this.entityManager.merge(t);
		this.entityManager.flush();
		this.detachEntity(t);
		return t;
	}

	@Override
	public void remove(T t) {
		this.entityManager.remove(t);
		this.entityManager.flush();
		this.detachEntity(t);

	}

	@Override
	public void truncate() {
		// TODO Auto-generated method stub

	}
}
