package py.com.tdn.dao;

import javax.ejb.Local;

import py.com.tdn.ras.model.VentaDetalle;

@Local
public interface VentaBean {
	void agregarDetalle(VentaDetalle ventaDetalle, Boolean guardar);
	void guardarCLiente(String nombre, String apellido, Boolean guardad);

}