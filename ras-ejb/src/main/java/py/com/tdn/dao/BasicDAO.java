package py.com.tdn.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

@Local
public interface BasicDAO<T, PK extends Serializable> {

	/**
	 * Sirve para sacar la entidad del tracking de jpa. Tambien pasa a null
	 * todas las listas contenidas dentro de la entidad
	 * 
	 * @param entity
	 * @return la entidad no trackeada
	 */
	public T detachEntity(T entity);

	/**
	 * Sirve para sacar las entidades del tracking de jpa. Tambien pasa a null
	 * todas las listas contenidas dentro de cada entidad
	 * 
	 * @param entities
	 * @return la entidades no trackeadas
	 */
	public List<T> detachEntities(List<T> entities);

	/**
	 * Persiste la entidad pasada como parametro
	 * 
	 * @param t
	 */
	public T persist(T t);

	

	/**
	 * Actualiza la entidad
	 * 
	 * @param t
	 *            Entidad
	 * @return
	 */
	public T merge(T t);

	/**
	 * Elimina el objeto T
	 * 
	 * @param t
	 * @return
	 */
	public void remove(T t);

	/**
	 * Elimina todas las entradas de la tabla que se corresponde con la entidad
	 */
	void truncate();

}
