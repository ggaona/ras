package py.com.tdn.dao;

import java.util.List;

import javax.ejb.Local;

import py.com.tdn.ras.model.Cliente;
import py.com.tdn.ras.model.*;

@Local
public interface ProcesoDAO {
	
	Proceso getByDescripcion(String descripcion);
	List<Cliente> getListaClientes(String nombreCliente);
	List<Cliente> getListaClientesConVenta();

}
