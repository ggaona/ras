package py.com.tdn.ras.metaModel;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import py.com.tdn.ras.model.Cliente;
import py.com.tdn.ras.model.Venta;

@Generated(value="Dali", date="2015-03-14T10:29:30.399-0300")
@StaticMetamodel(Cliente.class)
public class Cliente_ {
	public static volatile SingularAttribute<Cliente, Integer> identificador;
	public static volatile ListAttribute<Cliente, Venta> venta;
	public static volatile SingularAttribute<Cliente, String> nombre;
	public static volatile SingularAttribute<Cliente, String> Apellido;
	public static volatile SingularAttribute<Cliente, String> ruc;
}
