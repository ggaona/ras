package py.com.tdn.ras.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PRODUCTOS")

public class Producto {

	@Id
	@SequenceGenerator(name = "GEN_PRODUCTO", sequenceName = "SQ_PRODUCTO")
	private int identificador;
	
	@Column(name = "DESCRIPCION", length = 80)
	private String descripcion;

	public int getIdentificador() {
		return identificador;
	}

	public void setIdentificador(int identificador) {
		this.identificador = identificador;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
