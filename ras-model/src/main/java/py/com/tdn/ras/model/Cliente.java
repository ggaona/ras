package py.com.tdn.ras.model;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "CLIENTES")
public class Cliente {

	
	@Id
	@SequenceGenerator(name = "GEN_CLIENTE", sequenceName = "SQ_CLIENTE")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GEN_CLIENTE")
	private Integer identificador;
	
	@OneToMany(mappedBy = "cliente")
    List<Venta> venta;
	
	@Column(name="NOMBRE")
	private String nombre;
	
	@Column(name="APELLIDO")
	private String Apellido;
	
	@Column(name="RUC")
	private String ruc;

	public Integer getIdentificador() {
		return identificador;
	}

	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}

	public List<Venta> getVenta() {
		return venta;
	}

	public void setVenta(List<Venta> venta) {
		this.venta = venta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return Apellido;
	}

	public void setApellido(String apellido) {
		Apellido = apellido;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	
}
