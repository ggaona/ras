package py.com.tdn.ras.metaModel;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import py.com.tdn.ras.model.Cliente;
import py.com.tdn.ras.model.Venta;
import py.com.tdn.ras.model.VentaDetalle;

@Generated(value="Dali", date="2015-03-14T10:29:30.404-0300")
@StaticMetamodel(Venta.class)
public class Venta_ {
	public static volatile SingularAttribute<Venta, Integer> identificador;
	public static volatile SingularAttribute<Venta, Cliente> cliente;
	public static volatile ListAttribute<Venta, VentaDetalle> ventaDetalle;
	public static volatile SingularAttribute<Venta, Integer> montoTotal;
	public static volatile SingularAttribute<Venta, String> numeroComprobante;
}
