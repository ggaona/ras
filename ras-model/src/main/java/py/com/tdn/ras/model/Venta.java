package py.com.tdn.ras.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "VENTA")
public class Venta implements Serializable {

	private static final long serialVersionUID = 2L;

	@Id
	@SequenceGenerator(name = "GEN_VENTA", sequenceName = "SQ_VENTA")
	private Integer identificador;
	
	@JoinColumn (name = "ID_CLIENTE")
	@ManyToOne
    private Cliente cliente;
	
	@OneToMany(mappedBy = "venta")
    List<VentaDetalle> ventaDetalle;
	
	@Column(name = "MONTO_TOTAL")
	private Integer montoTotal;

	@Column(name = "NRO_COMPROBANTE", length = 15)
	private String numeroComprobante;

	public Integer getIdentificador() {
		return identificador;
	}

	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<VentaDetalle> getVentaDetalle() {
		return ventaDetalle;
	}

	public void setVentaDetalle(List<VentaDetalle> ventaDetalle) {
		this.ventaDetalle = ventaDetalle;
	}

	public Integer getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(Integer montoTotal) {
		this.montoTotal = montoTotal;
	}

	public String getNumeroComprobante() {
		return numeroComprobante;
	}

	public void setNumeroComprobante(String numeroComprobante) {
		this.numeroComprobante = numeroComprobante;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
