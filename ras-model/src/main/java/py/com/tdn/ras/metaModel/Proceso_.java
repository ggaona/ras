package py.com.tdn.ras.metaModel;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import py.com.tdn.ras.model.Proceso;

@Generated(value="Dali", date="2015-03-14T10:29:30.401-0300")
@StaticMetamodel(Proceso.class)
public class Proceso_ {
	public static volatile SingularAttribute<Proceso, Integer> codigoProceso;
	public static volatile SingularAttribute<Proceso, String> descripcion;
}
