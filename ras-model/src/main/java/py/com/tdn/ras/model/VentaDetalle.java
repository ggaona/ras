package py.com.tdn.ras.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder.In;

@Entity
@Table(name = "VENTA_DETALLE")
public class VentaDetalle implements Serializable {

	/**
	 *
	 * Entity implementation class for Entity: Proceso
	 * 
	 */

	@Id
	@SequenceGenerator(name = "GEN_VENTA_DETALLE", sequenceName = "SQ_VENTA_DETALLE")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GEN_VENTA_DETALLE")
	private Integer identificador;
	 
    @JoinColumn (name = "ID_COMPROBANTE")
    @ManyToOne
    private Venta venta;
	
	@Column(name = "CANTIDAD")
	private Integer cantidad;
	
	@Column(name = "MONTO")
	private Integer monto;

	public Integer getIdentificador() {
		return identificador;
	}

	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}

	public Venta getVenta() {
		return venta;
	}

	public void setVenta(Venta venta) {
		this.venta = venta;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getMonto() {
		return monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}
	
}
