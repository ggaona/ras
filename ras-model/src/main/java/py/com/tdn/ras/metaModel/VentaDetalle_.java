package py.com.tdn.ras.metaModel;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import py.com.tdn.ras.model.Venta;
import py.com.tdn.ras.model.VentaDetalle;

@Generated(value="Dali", date="2015-03-14T10:29:30.405-0300")
@StaticMetamodel(VentaDetalle.class)
public class VentaDetalle_ {
	public static volatile SingularAttribute<VentaDetalle, Integer> identificador;
	public static volatile SingularAttribute<VentaDetalle, Venta> venta;
	public static volatile SingularAttribute<VentaDetalle, Integer> cantidad;
	public static volatile SingularAttribute<VentaDetalle, Integer> monto;
}
