package py.com.tdn.ras.metaModel;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import py.com.tdn.ras.model.Producto;

@Generated(value="Dali", date="2015-03-14T10:29:30.403-0300")
@StaticMetamodel(Producto.class)
public class Producto_ {
	public static volatile SingularAttribute<Producto, Integer> identificador;
	public static volatile SingularAttribute<Producto, String> descripcion;
}
